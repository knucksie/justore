package ru.kpfu.itis.naxi.juststore.service;


import net.bytebuddy.implementation.auxiliary.AuxiliaryType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.kpfu.itis.naxi.juststore.model.Category;
import ru.kpfu.itis.naxi.juststore.model.Order;
import ru.kpfu.itis.naxi.juststore.model.Product;
import ru.kpfu.itis.naxi.juststore.model.State;
import ru.kpfu.itis.naxi.juststore.repository.CategoryRepository;
import ru.kpfu.itis.naxi.juststore.repository.OrderRepository;
import ru.kpfu.itis.naxi.juststore.repository.ProductRepository;

import java.util.Set;


@Service
public class AdminService {
    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    public Set<Order> getActiveOrders(){
        return orderRepository.getAllActiveOrders();
    }

    public void addProduct(Product product){
        productRepository.save(product);
    }

    public void formOrder(long orderId){
        Order order = orderRepository.getOne(orderId);
        order.setState(State.FORMED);
        orderRepository.save(order);
    }
}
