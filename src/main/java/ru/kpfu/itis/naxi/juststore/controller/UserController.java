package ru.kpfu.itis.naxi.juststore.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import ru.kpfu.itis.naxi.juststore.form.UserForm;
import ru.kpfu.itis.naxi.juststore.service.UserService;

import javax.servlet.http.HttpServletRequest;

@Controller
public class UserController {

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/index", method = RequestMethod.GET)
    public String test(ModelMap model) {
        return "index";
    }

    @GetMapping(value = "/signup")
    public String getSignUpForm(Model model) {
        model.addAttribute("form", new UserForm());
        return "register";
    }

    @PostMapping(value = "/signup")
    public String signUp(@ModelAttribute UserForm userForm) {
        System.out.println(userForm.getName());
        System.out.println(userForm.getSurname());
        System.out.println(userForm.getPassword());
        userService.saveUser(userForm);
        return "index";
    }


    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String loginPage(Authentication authentication, Model model, HttpServletRequest request) {
        if (authentication != null) {
            return "index";
        }
        if (request.getParameterMap().containsKey("error")) {
            model.addAttribute("error", true);
        }

        return "login";
    }

}
