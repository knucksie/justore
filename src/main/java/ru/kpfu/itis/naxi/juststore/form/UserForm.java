package ru.kpfu.itis.naxi.juststore.form;

import lombok.Data;
import lombok.Getter;
import org.springframework.stereotype.Component;

@Component
@Data
public class UserForm {
    private String name;
    private String surname;
    private String email;
    private String password;
}