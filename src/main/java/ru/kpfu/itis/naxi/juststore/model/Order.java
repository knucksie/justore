package ru.kpfu.itis.naxi.juststore.model;

import lombok.Builder;
import lombok.Data;
import org.hibernate.jdbc.BatchedTooManyRowsAffectedException;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

@Entity
@Data
@Builder
@Table(name = "justore_order")
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ManyToOne(fetch = FetchType.EAGER)
    private User user;

    private Date orderDate;

    @Column(name = "address")
    private String deliveryAddress;

    @OneToMany(fetch = FetchType.EAGER)
    private Set<Product> productSet;

    @Enumerated(value = EnumType.STRING)
    private State state;


}
