package ru.kpfu.itis.naxi.juststore.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.kpfu.itis.naxi.juststore.model.Product;

import java.util.Set;


@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {

}
