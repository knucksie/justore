package ru.kpfu.itis.naxi.juststore.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.kpfu.itis.naxi.juststore.model.Order;
import ru.kpfu.itis.naxi.juststore.model.Product;

import java.util.Set;

@Repository
public interface OrderRepository extends JpaRepository<Order, Long> {

    @Query(value = "select * from justore_order \n" +
            "where state = \"ACTIVE\"", nativeQuery = true)
    Set<Order> getAllActiveOrders();


}
