package ru.kpfu.itis.naxi.juststore.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.kpfu.itis.naxi.juststore.form.UserForm;
import ru.kpfu.itis.naxi.juststore.model.Role;
import ru.kpfu.itis.naxi.juststore.model.User;
import ru.kpfu.itis.naxi.juststore.repository.UserRepository;

@Service
public class UserService implements UserDetailsService {

    final
    UserRepository userRepository;
    final PasswordEncoder passwordEncoder;
    public UserService(@Autowired UserRepository userRepository, @Autowired PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        User user = userRepository.findByEmail(email);

        if (user == null) {
            throw new UsernameNotFoundException("User not found");
        }

        return user;
    }

    public void saveUser(UserForm form) {
        User userFromDB = userRepository.findByEmail(form.getEmail());

        if (userFromDB != null) {
            return;
        }

        String hashedPassword = passwordEncoder.encode(form.getPassword());
        System.out.println(hashedPassword);
        System.out.println(form.getName());
        System.out.println(form.getSurname());
        User user = User.builder()
                .name(form.getName())
                .surname(form.getSurname())
                .password(hashedPassword)
                .email(form.getEmail())
                .role(Role.USER)
                .build();
        userRepository.save(user);
    }


}
