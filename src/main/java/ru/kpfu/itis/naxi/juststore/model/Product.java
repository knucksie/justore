package ru.kpfu.itis.naxi.juststore.model;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ManyToOne(fetch = FetchType.EAGER)
    private Brand brand;

    @Column(length = 300)
    private String description;

    private Date lastModified;

    private int cost;


}
