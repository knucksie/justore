package ru.kpfu.itis.naxi.juststore.model;


import com.sun.xml.bind.v2.TODO;
import lombok.*;

import javax.annotation.sql.DataSourceDefinitions;
import javax.persistence.*;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import ru.kpfu.itis.naxi.juststore.model.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Set;


@Data
@Builder
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "justore_user")
public class User implements UserDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(value = EnumType.STRING)
    private Role role;

    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(name = "purchases", joinColumns = @JoinColumn(name = "user"),
    //foreign key for other side - EmployeeEntity in employee_car table
    inverseJoinColumns = @JoinColumn(name = "product"))
    private Set<Product> purchases;


    private String name;
    private String surname;

    @Transient
    private ArrayList<Product> bucket;

    @Column(unique = true)
    private String email;

    private String password;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Collections.singletonList(this.role);
    }

    @Override
    public String getPassword() {
        return this.password;
    }

    @Override
    public String getUsername() {
        return this.email;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
