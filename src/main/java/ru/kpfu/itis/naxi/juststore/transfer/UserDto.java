package ru.kpfu.itis.naxi.juststore.transfer;

import lombok.*;
import ru.kpfu.itis.naxi.juststore.model.User;

@Data
@AllArgsConstructor
@Builder
public class UserDto {
    private String email;
    private String name;
    private String surname;

    public static UserDto from(User user) {
        return UserDto.builder()
                .name(user.getName())
                .surname(user.getSurname())
                .email(user.getUsername())
                .build();
    }
}